class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if User.find_by_email(@user.email)
      flash.now[:error] = "Such user already exists"
      render :new
    elsif user_params['password'] != user_params['password_confirmation']
      flash.now[:error] = "Passwords do not match"
      render :new
    else
      if @user.save
        session[:user_id] = @user.id
        redirect_to '/'
      else
        redirect_to '/signup'
      end
    end
  end

  private
  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end
end
