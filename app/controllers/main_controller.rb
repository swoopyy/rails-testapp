class MainController < ApplicationController
  def set_user
    if !current_user
      redirect_to '/login'
      return
    end
    @user = current_user
  end

  def new
    @attached_file = AttachedFile.new
  end

  def index
    set_user
    @attached_files = AttachedFile.all
  end

  def create
    @attached_file = AttachedFile.new(file_params)
    if @attached_file.save
      redirect_to '/', notice: "The file #{@attached_file.name} has been uploaded."
    else
      render :new
    end
  end

  def destroy
    @attached_file = AttachedFile.find(params[:id])
    @attached_file.destroy
    redirect_to '/', notice:  "The file #{@attached_file.name} has been deleted."
  end

  private
  def file_params
    params.require(:attached_file).permit(:name, :attachment)
  end
end
