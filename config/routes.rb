Rails.application.routes.draw do
  get '/' => 'main#index'
  get '/new' => 'main#new'
  post '/create' => 'main#create'
  delete '/destroy' => 'main#destroy'
  get 'signup'  => 'users#new'
  resources :users
  get 'login' => 'sessions#new'
  resources :sessions
  post 'login' => 'sessions#create'
  get 'logout' => 'sessions#destroy'
  resources :main, only: [:index, :new, :create, :destroy]
  root "main#index"
end
