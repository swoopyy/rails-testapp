require 'test_helper'

class AttachedFilesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get attached_files_index_url
    assert_response :success
  end

  test "should get new" do
    get attached_files_new_url
    assert_response :success
  end

  test "should get create" do
    get attached_files_create_url
    assert_response :success
  end

  test "should get destroy" do
    get attached_files_destroy_url
    assert_response :success
  end

end
